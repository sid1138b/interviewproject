/**
 * The header to define the C++ entry point 
 */
#pragma once

namespace DoorSys
{
/**
 * The power-on entry point. I assume a BSP initialized the processor 
 * hardware and established the foundation to run C++ code. The
 * BSP would then call into this routine to start the application.
 */
extern void entryPoint();
}
