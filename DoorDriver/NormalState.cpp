/**
 * The power on state processor
 */
#include <stdio.h>
#include "DeviceState.h"
#include "DeviceInfo.h"

namespace DoorSys
{

    extern PowerOn poweron;
    extern Normal normal;
    extern DeviceState base;

    /**
     * put the system into the error state
     */
    inline void setErrorState()
    {
        systemError();
        base.setState(States::ERR);
    }

    /** 
     * Process the normal loop system. The normal system
     * assumes the door is closed and the arm is on
     */
    DeviceState* Normal::pollLoop()
    {
        if (readError())
        {
            setErrorState();
            return &base;
        }

        if (readDoor())
        {
            // Somehow the door got opened
            setErrorState();
            return &base;
        }

        if (readOpen())
        {
            setOff();
            base.setState(States::DOR_RON);
            return &base;
        }

        if (readClose())    // The close button is still pushed - ignore
        {
            setClose();
            // put a delay of a few ms while we poll error to make sure door is really closed
            clrClose();   // Don't need the door motor on after it is closed
        }

        // Still processing - so don't change
        return this;
    }

    /**
     * The change state routine allows the state processor to tweak the new state if it 
     * needs to
     */
    DeviceState* Normal::changeState(DeviceState* newState)
    {
        // No tweaking needed
        return newState;
    }

    /**
     * called if an error is detected while processing
     */
    DeviceState* Normal::handleError()
    {
        systemError();
        base.setState(States::ERR);
        return &base;
    }

    /**
     * Called when the API detects a reset command. Unlikely in the power on state
     * since this state is very short lived.
     */
    DeviceState* Normal::handleReset()
    {
        systemInit(true);
        base.setState(States::RESET);
        return &base;
    }

}