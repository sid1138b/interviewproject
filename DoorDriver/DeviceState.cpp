/**
 * classes to manage the state machine. This system only uses three classes, with the base class
 * handling most states. In the real system, each state would have its own class.
 */

#include <stdio.h>
#include "DeviceState.h"
#include "DeviceInfo.h"
#include "UISystem.h"
using namespace DoorSys;

namespace DoorSys
{

    /*----------------------------
        State Processors
    -----------------------------*/
    PowerOn poweron;
    Normal  normal;
    DeviceState base(States::UNKNOWN);

    static DeviceState* curProc = &poweron;        /// what state the state machine is processing
    static DeviceState* stateNew;
    /**
     * The devLoopFlag global is to work with the simulator so the simulator can stop
     * the door driver. In a real system this would not be used
     */
    bool DoorSys::devLoopFlag;

    /**
     * Get the state machine processing
     */
    DeviceState* createStateMachine()
    {
        DeviceState* ret = &poweron;
        stateNew = nullptr;
        curProc = ret;
        return ret;
    }

    States getCurStateId()
    {
        return curProc->getCurState();
    }

    /**
     * The processing loop is the main system. It loops forever processing the state machine
     */
    void processingLoop()
    {
        devLoopFlag = true;

        while (devLoopFlag)
        {
            checkUI();          // I was going to make this multi-threaded but ran out of time
            stateNew = curProc->pollLoop();
            if (stateNew->getCurState() != curProc->getCurState())
            {
                stateNew = curProc->changeState(stateNew);
            }
            if (readError())
            {
                stateNew = curProc->handleError();
            }
            if (curProc->getCurState() == States::RESET)
            {
                stateNew = curProc->handleReset();
            }

            if (stateNew->getCurState() != curProc->getCurState())
            {
                curProc = stateNew;     // Move into the new state.
            }
        }
    }


    /**
     * put the system into the error state
     */
    inline void setErrorState()
    {
        systemError();
        base.setState(States::ERR);
    }

    /** 
     * Process the normal loop system. The normal system
     * assumes the door is closed and the arm is on
     */
    DeviceState* DeviceState::pollLoop()
    {
        DeviceState* ret = this;

        /* Note - for this state machine processing I am only taking a simple path.
         * For example, I am not checking the door open or close requests until I am in the normal (DC_RON) state.
         * This was decided to make the state machine implementable in the time I had.
         */
        switch (ret->curState)
        {
        case States::INIT:
            clrOpen();
            setClose();
            clrOn();
            setOff();
            base.setState(States::DU_RU);       // Waiting for door to close and robot off
            ret = &base;                         // This would actually be another state object
            break;

        case States::DU_RU:
            if (!readDoor() && !readArm())
            {
                clrOff();       // Clear the control bits since the system is now running
                clrClose();
                base.setState(States::DC_ROFF);
                ret = &base;
            }
            // Would perform error checks here before leaving the state
            break;

        case States::DC_ROFF:
            // We should run a timer to make sure the state settles. I am not implementing timers in this example
            //...
            // Now we make sure all control bits are off before turning on the motor
            writeCtrl(0);
            setOn();        // Turn on the motor
            ret = &normal;
            ret->setState(States::DC_RON);
            break;

        case States::DC_RON:     // Handled in Normal
            break;

            // These states will return to normal - temp processing
        case States::DOR_RON:
            writeCtrl(0);
            setOn();
            setClose();
            ret = &normal;
            ret->setState(States::DC_RON);

            break;

        case States::DOR_ROFF:
            writeCtrl(0);
            setOn();
            setClose();
            ret = &normal;
            ret->setState(States::DC_RON);
            break;

        case States::DOG_ROFF:
            writeCtrl(0);
            setOn();
            setClose();
            ret = &normal;
            ret->setState(States::DC_RON);
            break;

        case States::DO_ROFF:
            writeCtrl(0);
            setOn();
            setClose();
            ret = &normal;
            ret->setState(States::DC_RON);
            break;


        case States::DCR_ROFF:
            writeCtrl(0);
            setOn();
            setClose();
            ret = &normal;
            ret->setState(States::DC_RON);
            break;


        case States::ERR:
            systemError();
            base.setState(States::ERR);
            break;
        }           // end switch


        if (readError())
        {
            setErrorState();
            return &base;
        }

        if (readDoor())
        {
            // Somehow the door got opened
            setErrorState();
            return &base;
        }

        if (readOpen())
        {
            setOff();
            base.setState(States::DOR_RON);
            return &base;
        }

        if (readClose())    // The close button is still pushed - ignore
        {
            setClose();
            // put a delay of a few ms while we poll error to make sure door is really closed
            clrClose();   // Don't need the door motor on after it is closed
        }

        // Still processing - so don't change
        return this;
    }

    /**
     * The change state routine allows the state processor to tweak the new state if it 
     * needs to
     */
    DeviceState* DeviceState::changeState(DeviceState* newState)
    {
        // No tweaking needed
        return newState;
    }

    /**
     * called if an error is detected while processing
     */
    DeviceState* DeviceState::handleError()
    {
        systemError();
        base.setState(States::ERR);
        return &base;
    }

    /**
     * Called when the API detects a reset command. Unlikely in the power on state
     * since this state is very short lived.
     */
    DeviceState* DeviceState::handleReset()
    {
        systemInit(true);
        base.setState(States::RESET);
        return &base;
    }

}


