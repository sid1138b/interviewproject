/**
 *  The entry module is the power-on and reset entry point
 *  It will initialize hardware and launch the control loop.
 */
#include <stdio.h>
#include "Entry.h"
#include "DeviceState.h"

using namespace DoorSys;
/**
 * The power-on entry point. I assume a BSP initialized the processor 
 * hardware and established the foundation to run C++ code. The
 * BSP would then call into this routine to start the application.
 */
void DoorSys::entryPoint()
{
    printf("Starting up door control system\n");
    createStateMachine();
    processingLoop();
}