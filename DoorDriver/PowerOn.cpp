/**
 * The power on state processor
 */
#include <stdio.h>
#include "DeviceState.h"
#include "DeviceInfo.h"

namespace DoorSys
{

    extern PowerOn poweron;
    extern Normal normal;
    extern DeviceState base;

    /** 
     * Process the power on loop system
     */
    DeviceState* PowerOn::pollLoop()
    {
        systemInit(false);
        base.setState(States::INIT);
        return &base;
    }

    /**
     * The change state routine allows the state processor to tweak the new state if it 
     * needs to
     */
    DeviceState* PowerOn::changeState(DeviceState* newState)
    {
        base.setState(States::INIT);
        return &base;
    }

    /**
     * called if an error is detected while processing
     */
    DeviceState* PowerOn::handleError()
    {
        systemError();
        base.setState(States::ERR);
        return &base;
    }

    /**
     * Called when the API detects a reset command. Unlikely in the power on state
     * since this state is very short lived.
     */
    DeviceState* PowerOn::handleReset()
    {
        systemInit(true);
        base.setState(States::RESET);
        return &base;
    }

}