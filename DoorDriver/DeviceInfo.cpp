/**
 * @brief Information about the door system device
 *
 * DeviceInfo contains information about the physical device. The device is a door control
 * system that manages access to a room with a robotic arm. The software assumes the following:
 *  - The arm is big enough to injure a person and maybe worse
 *  - The arm runs based on commands from another system. However, the door must ensure arm is off before door opens
 *  - The door can only be opened by the system or overridden by a key.
 *  - All buttons and switches are debounced by hardware so the software does not need to wait for key settling
 *  - Any error condition is critical and therefore the arm must stop
 *  - Uncontrolled door opening is a critical error and results in a arm shutdown
 */

#include <atomic>
#include <mutex>
#include "DeviceInfo.h"

using namespace std;

namespace DoorSys
{
    /**
     * The status byte would be located in some physical memory somewhere. The real system would use
     * defined pointers to that physical memory or a BSP or similar code to access the device.
     * I am taking a simpler route. Also, these would normally be volatile instead of atomic.
     */
    
    REG_ACCESS status;      /// The read only status register
    REG_ACCESS control;     /// The write only control register
    REG_ACCESS shadow;      /// The shadow copy of the control register
    static recursive_mutex intSim;    /// Simulate interrupt control with a mutex

    /**
     * Set the system into its safe mode - arm off, door open
     */
    void systemSafe()
    {
        writeCtrl(0);       // Clear all bits
        setOff();           // Turn off arm
        setOpen();          // Open the door

        printf("Set the system into safe mode, status = 0x%02X, control = 0X%02X\n", readStatus(), readCtrl());
    }

    /**
     * Put the system into error
     */
    void systemError()
    {
        printf("Detected error condition, status = 0x%02X, control = 0X%02X\n", readStatus(), readCtrl());
        systemSafe();
        errOn();            // Turn on alarm;
    };



    /**
    * Initialize the hardware. Currently there is no difference between power on or reset command
    *
    * @param reset - flag to indicate if this call came from a reset - not used
    */
    void systemInit(bool reset)
    {
        printf("Performing system init\n");
        disableInt();
        atomic_store(&status, 0);
        atomic_store(&control, 0);
        atomic_store(&shadow, 0);
        systemSafe();
        enableInt();
    }

    /*----------------------------------------------------------------
     * Read status register
     *---------------------------------------------------------------*/
    /**
    * Read the status register to determine the state of the system
    */
    BYTE readStatus() 
    { 
#ifdef USING_SIM  // This shows using or not using the simulator. It would be better in the header as a macro or inline
                  // For the rest of the code I will assume simulator to make it simpler
       return atomic_load(&status); 
#else
        return status;
#endif
    };

    bool readBit(StatusBits bit) 
    {
        BYTE reg = readStatus(); 
        return ((reg & bit) != 0);
    }


    /*----------------------------------------------------------------
     * Write control register
     *---------------------------------------------------------------*/

    /**
    * Write the full 8 bits of the control register
    * @param bits the 8 bits to write
    */
    void writeCtrl(BYTE bits)
    {
        disableInt();
        atomic_store(&shadow, bits);
        atomic_store(&control, bits);
        enableInt();
    }

    /**
    * Set the control bit
    * @param bit the mask of the bit to set
    * @return the current state of the control bits 
    */
    BYTE setCtrl(CtrlBits bit)
    {
        disableInt();
        BYTE byte = atomic_fetch_or(&shadow, bit);
        writeCtrl(byte);
        enableInt();
        return byte;
    }

    /**
    * clear the control bits
    * @param bit the mask of the bit to clear
    * @return the current state of the control bits
    */
    BYTE clrCtrl(CtrlBits bit)
    {
        disableInt();
        BYTE byte = atomic_fetch_and(&shadow, !bit);
        writeCtrl(byte);
        enableInt();
        return byte;
    }

    /**
    * Read the control register shadow value
    * @return the current shadow value
    */
    BYTE readCtrl()
    {
        return atomic_load(&shadow);
    }

    //-------- Specific bit set and clear functions -------------
    void setOpen()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.open = 1;
        writeCtrl(reg.byte);
        enableInt();
    }


    void clrOpen()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.open = 0;
        writeCtrl(reg.byte);
        enableInt();
    }

    void setClose()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.close = 1;
        writeCtrl(reg.byte);
        enableInt();
    }

    void clrClose()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.close = 0;
        writeCtrl(reg.byte);
        enableInt();
    }

    void setOff()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.armOff = 1;
        writeCtrl(reg.byte);
        enableInt();
    }

    void clrOff()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.armOff = 0;
        writeCtrl(reg.byte);
        enableInt();
    }

    void setOn()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.armOn = 1;
        writeCtrl(reg.byte);
        enableInt();
    }

    void clrOn()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.armOn = 1;
        writeCtrl(reg.byte);
        enableInt();
    }

    void errOn()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.error = 1;
        writeCtrl(reg.byte);
        enableInt();
    }

    void errOff()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.error = 0;
        writeCtrl(reg.byte);
        enableInt();
    }

    void doReset()
    {
        disableInt();
        CtrlUnion reg;
        reg.byte = atomic_load(&shadow);
        reg.reg.open = 1;
        writeCtrl(reg.byte);
        enableInt();
    }

    /**
    * Disable interrupts by grabbing the lock. In the real system, the disable would happen
    * immediately
    */
   void disableInt()
   {
       intSim.lock();

   }
    /**
    * Enable interrupts by releasing the lock. In the real system enabling would
    * entail setting a bit in a control register
    */
    void enableInt()
    {
        intSim.unlock();
    }

}
