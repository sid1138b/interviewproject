/**
 * The DeviceInfo.h file defines the various bits and controls for the hardware device.
 * This would also be the place to put machine or project specific definitions that
 * may use conditional compiling to support multiple platforms.
 */
#pragma once

#ifndef BYTE
#define BYTE unsigned char
#endif

#define USING_SIM
#ifdef USING_SIM
#include <atomic>
#include <mutex>
#define REG_ACCESS std::atomic_uint8_t
#else
#define REG_ACCESS volatile BYTE
#endif


namespace DoorSys
{

    extern REG_ACCESS status;
    extern REG_ACCESS control;
    extern REG_ACCESS shadow;
    
    /**
     * The status word is a read-only hardware register that has information about the state of the
     * door system
     */
    enum StatusBits 
    {
        DOOR    = 0x01,         /// Door status mask
        ARM     = 0x02,         /// Arm active mask
        OPEN    = 0x04,         /// Open request mask
        CLOSE   = 0x08,         /// Close request mask
        ERROR   = 0x80          /// Error active mask
    };

    /**
     * The status register contains the bits the system can read to determine the status of the
     * door control system. A set bit will also cause an interrupt. The register is read-only
     */
    struct StatusReg
    {
        unsigned short door     : 1;        /// Door status - 1 = open
        unsigned short arm      : 1;        /// Arm status - 1 = active
        unsigned short open     : 1;        /// Door open request status - 1 = door open request 
        unsigned short close    : 1;        /// Door close request status - 1 = door close request
        unsigned short reserved : 3;        /// Three unused bits
        unsigned short error    : 1;        /// An error condition is happening - 1 = error
    };

    union StatUnion
    {
        BYTE byte;
        StatusReg reg;
    };

    /* 
     * The read and write code could be done with a macro or an inline if speed were a factor
     * I decided to use both inline and separate routines to show more structure for this processing
     */
    extern BYTE readStatus();
    extern bool readBit(StatusBits bit);
    inline bool readDoor()  { StatUnion u; u.byte = readStatus(); return u.reg.door; };
    inline bool readArm()   { StatUnion u; u.byte = readStatus(); return u.reg.arm; };
    inline bool readOpen()  { StatUnion u; u.byte = readStatus(); return u.reg.open; };
    inline bool readClose() { StatUnion u; u.byte = readStatus(); return u.reg.close; };
    inline bool readError() { StatUnion u; u.byte = readStatus(); return u.reg.error; };


    /** 
     * The control bits allow the software to control the door system. The control register is 
     * an 8-bit write-only register. Therefore there will be a shadow control register to record
     * what was written so the software knows the state of the control reg. This enum gives the 
     * the bitmask for each control bit.
     */
    enum CtrlBits
    {
        OPENR   = 0x01,         /// Open the door
        CLOSER  = 0x02,         /// Close the door
        ARM_OFF = 0x04,         /// Deactivate the robot arm
        ARM_ON  = 0x08,         /// Reactivate the arm
        ALARM   = 0x80          /// Turn on or off the alarm
    };

    /**
     * The control register allows the program to control the door system. Setting the bit will activate 
     * the specified device. Clearing the bit will typically not do anything, but most bits need to get 
     * when the state changes. The software will ensure certain exclusions.
     * Opening door will set close to 0. Closing the door will set open to 0.
     * The open cannot happen until the arm is stopped.
     * The arm cannot start until the door is closed. Deactivating arm clears the reactive bit.
     * An alarm deactivates the robot and opens the door.
     * 
     * The initial state is: Door closed. Arm off, no error.
     */
    struct CtrlReg
    {
        unsigned short open     : 1;    /// Set to one to activate the door open (unlock)
        unsigned short close    : 1;    /// set to one to activate the door closed (lock)
        unsigned short armOff   : 1;    /// Turn off the robot arm
        unsigned short armOn    : 1;    /// Turn on the robot arm
        unsigned short reserved : 3;    /// three unused bits
        unsigned short error    : 1;    /// Turn on the alarm bit
    };

    union CtrlUnion
    {
        BYTE byte;
        CtrlReg reg;
    };

    extern void writeCtrl(BYTE bits);
    extern BYTE setCtrl(CtrlBits bit);
    extern BYTE clrCtrl(CtrlBits bit);
    extern BYTE readCtrl();     // Read the control bit shadow register
    extern void setOpen();      // Set the door open command
    extern void clrOpen();
    extern void setClose();
    extern void clrClose();
    extern void setOff();
    extern void clrOff();
    extern void setOn();
    extern void clrOn();
    extern void errOn();
    extern void errOff();
    extern void doReset();

    extern void systemInit(bool reset);
    extern void systemError();

    // Simulate the interrupt system
    extern std::recursive_mutex intSim;
    /**
    * Disable interrupts by grabbing the lock. In the real system, the disable would happen
    * immediately
    */
   extern void disableInt();
    /**
    * Enable interrupts by releasing the lock. In the real system enabling would
    * entail setting a bit in a control register
    */
    extern void enableInt();



    /**
     * The interrupt processing routine. This routine would get entered
     * when an interrupt signal occurred. However, I ran out of time and did not 
     * implement this and went with a polling solution instead
     */
    inline void interrupt() { /* Process the interrupt request */ };
}           // end namespace