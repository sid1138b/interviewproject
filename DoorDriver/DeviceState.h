/**
 * This file defines the state machine system. 
 */
#pragma once

namespace DoorSys
{
    /** 
     * The different states
     * The state names depend on the values of the variables and timers
     *  - DO    door opened
     *  - DOR   door open request
     *  - DOG   door opening
     *  - DC    door closed
     *  - DCG   door Closing
     *  - DCR   door closing request
     *  - DU    door in unknown state (after power on init or reset) assume opened
     *  - ROFF  robot off
     *  - RON   robot on
     *  - RU    robot in unknown state
     *  - ERR   error detected
     *  - RST   reset
     */
    enum class States
    {
        POWER = 0,      /// Power on state
        INIT    ,       /// Initializing
        RESET   ,       /// Reset after error or user request
        DU_RU   ,       /// Door and robot in an unknown or uncontrolled state
        DC_ROFF ,       /// The door is closed and robot is off
        DC_RON  ,       /// Door Closed robot on (normal running condition_
        DOR_RON ,       /// Door Open Request, robot is on
        DOR_ROFF,       /// Door open request, robot off
        DOG_ROFF,       /// Door opening
        DO_ROFF ,       /// Door opened
        DCR_ROFF,       /// Door closing request
        ERR     ,       /// Door closed, robot unknown
        UNKNOWN         /// Don't know the state (should never happen)
    };

    extern bool loopflag;   /// This is only for the simulator
    /**
     * The virtual base class for states
     */
    class DeviceState
    {
    protected:
        States curState;        /// The current state for this process - used if the process handles more than
                                // one state
    public:
        DeviceState(States cur = States::UNKNOWN) : curState(cur) {};
        virtual ~DeviceState(void) {curState = States::UNKNOWN;};

        virtual DeviceState* pollLoop();
        virtual DeviceState* changeState(DeviceState* newState);
        virtual DeviceState* handleError();
        virtual DeviceState* handleReset();

        inline States getCurState() { return curState; };
        inline void setState(States state) { curState = state; }    // Used because we have this state process handling many states instead of one
        DeviceState* procDU_RU();

    };


    /**
     * Sample state machine class - power on reset state
     */
    class PowerOn : public DeviceState
    {
    public:
        PowerOn() : DeviceState(States::POWER) {};
        DeviceState* pollLoop();
        DeviceState* changeState(DeviceState* newState);
        DeviceState* handleError();
        DeviceState* handleReset();
    };

    /**
     * Sample state machine class - Normal running state
     */
    class Normal : public DeviceState
    {
    public:
        Normal() : DeviceState(States::DC_RON) {};
        DeviceState* pollLoop();
        DeviceState* changeState(DeviceState* newState);
        DeviceState* handleError();
        DeviceState* handleReset();
    };

    extern DeviceState* createStateMachine();
    extern void processingLoop();   // This routine loops forever
    extern bool devLoopFlag;
    extern States getCurStateId();

};      // End namespace DoorSys