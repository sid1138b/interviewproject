# HW device description

You are given a HW device that controls access to an area that is guarded by a door.

The user can press a button to request access to the area.

On seeing that the user is requesting access, the area is first cleared of the robotic arm operating in the area. The door is then opened.

The user finishes what he needs to do and presses the button to close the door. The door is then closed.

When the door is fully closed, the robotic arm activity is resumed.

The device has the following registers.

```
@status (Read only)
     bit 0: 0 => door closed,                           1 => door open
     bit 1: 0 => robot arm not active,                  1 => robot arm active
     bit 2: 0 => door open request button not pressed,  1 => door open request button pressed
     bit 3: 0 => door close request button not pressed, 1 => door close request button pressed
     bit 7: 0 => no error,                              1 => some error condition

@control (Write only)
     bit 0: set to 1 to open the door
     bit 1: set to 1 to close the door
     bit 2: set to 1 to deactivate the robot arm
     bit 3: set to 1 to reactivate the robot arm
     bit 7: set to 1 to raise an alarm to summon tech,  set to 0 to clear the error / alarm
```

The device has a single interrupt line. The device raises interrupts as follows.

1. Raises an interrupt when the user presses either the door open or door close buttton.
1. Raises an interrupt when the door is fully open or fully closed.
1. Raises an interrupt when the robotic arm is fully deactivated or fully reactivated.
1. Raises an interrupt when an error is detected.

## Driver

1. Implements the required behavior.
2. Raises the tech alarm when an error is detected.
3. Provides an API that clears the error (and tech alarm).

## Simulator

1. Lets you test the driver by supplying the required stimulus / responses.

# Instructions

You are asked to implement the driver and submit the code for review as a Bitbucket pull request **prior to the interview**. You will have to emulate the HW device in some way (maybe a C++ class). You may also need to find some way to simulate the HW to enable testing of your driver.

Proceed in following steps.

1. The description may have gaps and you may have questions. Please get clarifications via email, Teams, Zoom or phone.
1. Formulate a design and validate it in a short conversation.
1. Implement and test the driver.
1. Demo the driver and have your code reviewed in a panel **after the interview**.

# Expectations

1. You may ask as many questions as you like anytime during the exercise.
1. It is **fine not to finish** the exercise. This is mostly about getting an idea about how you approach problems, communicate and execute.