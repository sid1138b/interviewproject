#include <stdio.h>
#include <conio.h>
#include "UISystem.h"
#include "DeviceInfo.h"
#include "DeviceState.h"

namespace DoorSys
{

    static UISystem theUI;

    /** 
     * Routine to create and initialize a UI system class
     */
    void initUI()
    {
        theUI.doMenu();
    }

    /**
     * Routine to check if the UI has any commands
     */
    void checkUI()
    {
        theUI.procKey();
    }

    UISystem::UISystem(void)
    {
    }


    UISystem::~UISystem(void)
    {
    }

    void UISystem::startUI(void)
    {
        printf("Simulator UI started \n");
    }


    inline void writeStat(BYTE byte) { atomic_store(&status, byte); };
    inline void setDoor()   { StatUnion u; u.byte = readStatus(); u.reg.door = 1; writeStat(u.byte); };
    inline void clrDoor()   { StatUnion u; u.byte = readStatus(); u.reg.door = 0; writeStat(u.byte); };
    inline void setArm()    { StatUnion u; u.byte = readStatus(); u.reg.arm = 1; writeStat(u.byte); };
    inline void clrArm()    { StatUnion u; u.byte = readStatus(); u.reg.arm = 0; writeStat(u.byte); };
    inline void setUOpen()   { StatUnion u; u.byte = readStatus(); u.reg.open = 1; writeStat(u.byte); };
    inline void clrUOpen()   { StatUnion u; u.byte = readStatus(); u.reg.open = 0; writeStat(u.byte); };
    inline void setUClose()  { StatUnion u; u.byte = readStatus(); u.reg.close = 1; writeStat(u.byte); };
    inline void clrUClose()  { StatUnion u; u.byte = readStatus(); u.reg.close = 0; writeStat(u.byte); };
    inline void setError()  { StatUnion u; u.byte = readStatus(); u.reg.error = 1; writeStat(u.byte); };
    inline void clrError()  { StatUnion u; u.byte = readStatus(); u.reg.error = 0; writeStat(u.byte); };

    /**
     * Routine checks to see if there are any characters in the buffer and if so, processes them.
     */

    void UISystem::procKey(void)
    {
        while (_kbhit() != 0)
        {
            int key =_getch();
            switch (key)
            {
            case '?':
            case 'h':
            case 'H':
                doMenu();
                break;

            case 'x':
            case 'X':
            case 0x1b:      // Escape
                doExit();
                break;          // will never get hear

            case 'b':
            case 'B':
                printf("\n\n\n\n\n");
                break;

            case 'q':
            case 'Q':
                writeStat(0);
                break;

            default:
                procCommand(key);
                break;

            }           // end switch
        }           // end while _kbhit
    }


    void UISystem::doExit()
    {
        printf("UI and system exiting\n");
        std::exit(0);
    }

    void UISystem::doMenu(void)
    {
        printf("\n\n\n");
        printf("The simulator command set\n");
        printf("     ?, H       Help Menu\n");
        printf("     X, <ESC>   Exit\n");
        printf("     S          Display status bits\n");
        printf("     Z          Display the control bits\n");
        printf("     E          Display processor state\n");
        printf("     4          restart the state machine\n\n");

        printf("     O          Set the Door open bit\n");
        printf("     C          Clear the Door open bit\n\n");
    
        printf("     R          Set Robot Active bit\n");
        printf("     T          Clear the Robot Active bit\n\n");
    
        printf("     D          Set door open request\n");
        printf("     F          Clear Door Open Request\n\n");
    
        printf("     K          Set the door closed request bit\n");
        printf("     L          Clear the door closed bit\n\n");
    
        printf("     W          Set the error bit\n");
        printf("     A          Clear the error\n\n");
    
        printf("     Q          Clear all bits\n");
        printf("     B          insert 5 blank lines\n");
        printf("Note the system does not echo selection\n\n");
    }

    void UISystem::procCommand(char cmd)
    {
        BYTE reg1;
        BYTE reg2;
        switch (cmd)
        {
        case 's':
        case 'S':
            reg1 = atomic_load(&status);
            printf("Status Register:    0x%02X\n", reg1);
            break;

        case 'z':
        case 'Z':
            reg1 = atomic_load(&control);
            reg2 = atomic_load(&shadow);
            printf("Control register:   0x%02X    Shadow reg:   0x%02X\n", reg1, reg2);
            break;

        case 'e':
        case 'E':
            printf("Current State ID:   %d\n", getCurStateId());
            break;

        case '4':
            createStateMachine();
            break;

        case 'o':
        case 'O':
            setDoor();
            break;

        case 'c':
        case 'C':
            clrDoor();
            break;

        case 'r':
        case 'R':
            setArm();
            break;

        case 'T':
        case 't':
            clrArm();
            break;

        case 'd':
        case 'D':
            setUOpen();
            break;

        case 'F':
        case 'f':
            clrUOpen();
            break;

        case 'k':
        case 'K':
            setUClose();
            break;

        case 'l':
        case 'L':
            clrUClose();
            break;

        case 'w':
        case 'W':
            setError();
            break;

        case 'a':
        case 'A':
            clrError();
            break;

        default:
            printf("\nUnknown command of %c\n", cmd);
            break;
        }           // End switch
    }

}