#pragma once
#include "DeviceInfo.h"

namespace DoorSys
{
    extern void initUI();
    extern void checkUI();

    class UISystem
    {
    public:
        UISystem(void);
        ~UISystem(void);
        void startUI(void);
        void procKey(void);
        void doMenu(void);
        void doExit();
        void procCommand(char cmd);
    };

}